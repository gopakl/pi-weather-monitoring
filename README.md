**Pi Weather Monitoring**

This a project meant to utilize the environment canada API for the weather data of a city. 
The idea is to run this on a Raspberry Pi so as to get weather alerts, pertaining to not 
just the forecast in the day but also any dangers that have to be known. This would be used
in conjuntion with a push notification app on iOS like Pushover notifications so that one could
see this anywhere.