
from urllib.request import urlopen
from xml.etree.ElementTree import ElementTree

def fetchCityWeather(cityUrl):
    baseUrl = "http://dd.weather.gc.ca/citypage_weather/xml/"
    province = "ON/"

    try:
        weatherData = urlopen(baseUrl + province + cityUrl)
    except IOError:
        raise IOError("I'm afraid I can't let you do that")

    treeData = ElementTree().parse(weatherData)

    region = treeData[3][3].text

    print ("Data fetched for " + region)

    warnings = treeData[4]

    if (len(list(treeData[4])) == 0):
        print ("No warnings issued for the city")

fetchCityWeather("s0000458_e.xml")
fetchCityWeather("s0000786_e.xml")